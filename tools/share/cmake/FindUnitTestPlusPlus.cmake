#
# This CMake Module locates the UnitTest++ (http://unittest-cpp.sourceforge.net/)
# C++ unit testing framework, enabling FIND_PACKAGE(UnitTestPlusPlus) to work. 
#

find_path (UnitTestPlusPlus_INCLUDE_DIR NAMES unittest++/UnitTest++.h UnitTest++.h)
mark_as_advanced (UnitTestPlusPlus_INCLUDE_DIR)

find_library (UnitTestPlusPlus_LIBRARY NAMES UnitTest++)
mark_as_advanced (UnitTestPlusPlus_LIBRARY)

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (UnitTestPlusPlus DEFAULT_MSG UnitTestPlusPlus_LIBRARY UnitTestPlusPlus_INCLUDE_DIR)

if ("${UnitTestPlusPlus_INCLUDE_DIR}" MATCHES "NOTFOUND")
  set (UnitTestPlusPlus_LIBRARY)
  set (UnitTestPlusPlus_INCLUDE_DIR)
elseif ("${UnitTestPlusPlus_LIBRARY}" MATCHES "NOTFOUND")
  set (UnitTestPlusPlus_LIBRARY)
  set (UnitTestPlusPLus_INCLUDE_DIR)
else ("${UnitTestPlusPlus_INCLUDE_DIR}" MATCHES "NOTFOUND")
  set (UnitTestPlusPlus_FOUND 1)
  set (UnitTestPlusPlus_LIBRARIES ${UnitTestPlusPlus_LIBRARY})
  set (UnitTestPlusPlus_INCLUDE_DIRS ${UnitTestPlusPlus_INCLUDE_DIR})
endif ()
