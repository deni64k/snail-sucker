#pragma once

#include <cstring>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include <log4cxx/logger.h>
#include <log4cxx/level.h>
#include <log4cxx/patternlayout.h>

#include "image_bus.hxx"
#include "errors.hxx"
#include "socket_utils.hxx"

class ServerTCP {
public:
  ServerTCP(ImageBus &image_bus, int const port)
  : logger_(log4cxx::Logger::getLogger("ServerTCP"))
  , image_bus_(image_bus)
  , port_(port)
  {
    logger_->setLevel(log4cxx::Level::getInfo());
    logger_->addAppender(new log4cxx::ConsoleAppender(new log4cxx::PatternLayout("%d{%Y-%m-%d %H:%M:%S} %-5p %c %x - %m%n")));
  }

  void start_listen() {
    fd_ = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd_ == -1) {
      ErrnoError(logger_, "Could not create socket");
    }
    
    int const one = 1;
    if (::setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one)) == -1)
      LOG4CXX_WARN(logger_, "Setting SO_REUSEADDR on socket was failed");
    
    struct sockaddr_in addr_server;
    ::memset(reinterpret_cast<char *>(&addr_server), 0, sizeof(addr_server));
    addr_server.sin_family      = AF_INET;
    addr_server.sin_port        = htons(port_);
    addr_server.sin_addr.s_addr = htonl(INADDR_ANY);
    if (::bind(fd_, reinterpret_cast<struct sockaddr *>(&addr_server), sizeof(addr_server)) == -1) {
      ErrnoError(logger_, "Could not bind socket");
    }

    if (::listen(fd_, 5) < 0) {
      ErrnoError(logger_, "Could not listen socket");
    }

    LOG4CXX_INFO(logger_, "Listen on 0.0.0.0:" << port_);
  }

  void operator () () {
    start_listen();

    for (;;) {
      fd_set fds;
      struct timeval tv;
      int r, fd_max = fd_;

      FD_ZERO(&fds);
      FD_SET(fd_, &fds);
      for (auto const &client : clients_) {
        if (fd_max < client)
          fd_max = client;
        FD_SET(client, &fds);
      }

      tv.tv_sec  = 10;
      tv.tv_usec = 0;

      r = ::select(fd_max + 1, &fds, NULL, NULL, &tv);

      if (r == 0)
        continue;

      if (r == -1) {
        if (errno == EINTR)
          continue;

        ErrnoError(logger_, "::select failed");
      }

      if (FD_ISSET(fd_, &fds)) {
        int client;
        struct sockaddr_in addr_client;
        socklen_t alen = sizeof(addr_client);

        client = ::accept(fd_, reinterpret_cast<struct sockaddr *>(&addr_client), &alen);
        /* we may break out of accept if the system call */
        /* was interrupted. In this case, loop back and */
        /* try again */
        if (client < 0 && errno != ECHILD && errno != ERESTART && errno != EINTR)
          ErrnoError(logger_, "::accept failed");

        auto const ipstr = SocketUtils::get_ipstr_from_socket(client);
        LOG4CXX_INFO(logger_, "Accepted new connection from " << ipstr)
        clients_.push_back(client);
        image_bus_.add_subscriber(Subscriber(client, ipstr));
      }

      for (auto const &client : clients_) {
        if (FD_ISSET(client, &fds)) {
          int param = 1;
          if (FD_ISSET(client, &fds)) {
            ::ioctl(client, FIONREAD, &param);

            if (r == -1 || param == 0) {
              LOG4CXX_INFO(this->logger_, "Close connection with " << SocketUtils::get_ipstr_from_socket(client));
              image_bus_.remove_subscriber(client);
              ::shutdown(client, SHUT_RDWR);
              ::close(client);

              auto iter = std::remove(clients_.begin(), clients_.end(), client);
              if (iter != clients_.end())
                clients_.erase(iter);
            }
          }
        }
      }
    }
  }

private:
  log4cxx::LoggerPtr logger_;

  ImageBus &image_bus_;
  int fd_;
  int port_;
  int backlog_ = 5;
  
  std::vector<int> clients_;
};
