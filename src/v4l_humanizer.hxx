#pragma once

#include <boost/preprocessor.hpp>

class V4LHumanizer {
public:
  static std::string humanize_capability(v4l2_capability const &cap) {
    std::stringstream oss;

    oss << "Driver: " << cap.driver << '\n';
    oss << "Card: " << cap.card << '\n';
    oss << "Bus info: " << cap.bus_info << '\n';
    oss << "Version: " << ((cap.version >> 16) & 0xFF) << '.' << ((cap.version >> 8) & 0xFF) << '.' << (cap.version & 0xFF) << '\n';

    oss << "Capabilities: 0x" << std::hex << cap.capabilities << '\n';
    #define CAPABILITY_(x) (x, #x)
    #define CAPABILITIES_                          \
      (CAPABILITY_(V4L2_CAP_VIDEO_CAPTURE))        \
      (CAPABILITY_(V4L2_CAP_VIDEO_CAPTURE_MPLANE)) \
      (CAPABILITY_(V4L2_CAP_VIDEO_OUTPUT))         \
      (CAPABILITY_(V4L2_CAP_VIDEO_OUTPUT_MPLANE))  \
      (CAPABILITY_(V4L2_CAP_VIDEO_M2M))            \
      (CAPABILITY_(V4L2_CAP_VIDEO_M2M_MPLANE))     \
      (CAPABILITY_(V4L2_CAP_VIDEO_OVERLAY))        \
      (CAPABILITY_(V4L2_CAP_VBI_CAPTURE))          \
      (CAPABILITY_(V4L2_CAP_VBI_OUTPUT))           \
      (CAPABILITY_(V4L2_CAP_VBI_OUTPUT))           \
      (CAPABILITY_(V4L2_CAP_SLICED_VBI_OUTPUT))    \
      (CAPABILITY_(V4L2_CAP_RDS_CAPTURE))          \
      (CAPABILITY_(V4L2_CAP_VIDEO_OUTPUT_OVERLAY)) \
      (CAPABILITY_(V4L2_CAP_HW_FREQ_SEEK))         \
      (CAPABILITY_(V4L2_CAP_RDS_OUTPUT))           \
      (CAPABILITY_(V4L2_CAP_TUNER))                \
      (CAPABILITY_(V4L2_CAP_AUDIO))                \
      (CAPABILITY_(V4L2_CAP_RADIO))                \
      (CAPABILITY_(V4L2_CAP_MODULATOR))            \
      (CAPABILITY_(V4L2_CAP_READWRITE))            \
      (CAPABILITY_(V4L2_CAP_STREAMING))            \
      (CAPABILITY_(V4L2_CAP_ASYNCIO))              \
      (CAPABILITY_(V4L2_CAP_DEVICE_CAPS))

    #define CAPABILITY_TO_OSS_(r, data, elem)   \
      if (cap.capabilities & BOOST_PP_TUPLE_ELEM(2, 0, elem)) \
        (data) << "\t+" BOOST_PP_TUPLE_ELEM(2, 1, elem) "\n"; \
      else                                     \
        (data) << "\t-" BOOST_PP_TUPLE_ELEM(2, 1, elem) "\n";

    BOOST_PP_SEQ_FOR_EACH(CAPABILITY_TO_OSS_, oss, CAPABILITIES_);

    #undef CAPABILITY_
    #undef CAPABILITIES_
    #undef CAPABILITIES_TO_OSS_
    
    return oss.str();
  }

private:
};
