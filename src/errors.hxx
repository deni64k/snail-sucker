#pragma once

#include <stdexcept>

#include <errno.h>

#include <log4cxx/logger.h>

struct ErrnoError
: std::runtime_error
{
  ErrnoError(log4cxx::LoggerPtr logger, char const *msg)
  : std::runtime_error(msg)
  {
    LOG4CXX_FATAL(logger, msg << " error " << errno << ", " << ::strerror(errno));
    throw *this;
  }
};
