/*
 *  V4L2 video capture example
 *
 *  This program can be used and distributed without restrictions.
 *
 *      This program is provided with the V4L2 API
 * see http://linuxtv.org/docs.php for more information
 */

#pragma once
 
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <memory>
#include <stdexcept>
#include <string>

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <log4cxx/logger.h>
#include <log4cxx/level.h>
#include <log4cxx/patternlayout.h>

extern "C" {
  #include <linux/videodev2.h>
}

#include "buffer.hxx"
#include "image_bus.hxx"
#include "v4l_humanizer.hxx"

#ifndef V4L2_PIX_FMT_H264
#define V4L2_PIX_FMT_H264 v4l2_fourcc('H', '2', '6', '4') /* H264 with start codes */
#endif

enum class IOMethod {
  READ,
  MMAP,
  USERPTR
};

class Sucker {
public:
  Sucker(std::string const &dev_name,
         IOMethod    const  io_method,
         ImageBus          &image_bus)
  : logger_(log4cxx::Logger::getLogger("Sucker"))
  , dev_name_(dev_name)
  , io_method_(io_method)
  , image_bus_(image_bus)
  {
    logger_->setLevel(log4cxx::Level::getInfo());
    logger_->addAppender(new log4cxx::ConsoleAppender(new log4cxx::PatternLayout("%d{%Y-%m-%d %H:%M:%S} %-5p %c %x - %m%n")));
  }

  void open_device() {
    struct stat st;

    if (stat(dev_name_.c_str(), &st) == -1) {
      LOG4CXX_FATAL(logger_, "Cannot identify '" << dev_name_ << "': " << errno << ", " << strerror(errno));
      exit(EXIT_FAILURE);
    }

    if (!S_ISCHR(st.st_mode)) {
      LOG4CXX_FATAL(logger_, dev_name_ << " is no device");
      exit(EXIT_FAILURE);
    }

    fd_ = open(dev_name_.c_str(), O_RDWR /* required */ | O_NONBLOCK, 0);

    if (fd_ == -1) {
      LOG4CXX_FATAL(logger_, "Cannot open '" << dev_name_ << "': " << errno << ", " << strerror(errno));
      exit(EXIT_FAILURE);
    }

    LOG4CXX_INFO(logger_, "Device '" << dev_name_ << "' was opened successfully");
  }

  void close_device() {
    if (close(fd_) == -1)
      errno_throw_("close");

    LOG4CXX_INFO(logger_, "Device '" << dev_name_ << "' was closed successfully");
  }

  void init_device() {
    v4l2_capability cap;
    v4l2_cropcap    cropcap;
    v4l2_crop       crop;
    v4l2_format     fmt;
    unsigned        min;

    if (xioctl_(fd_, VIDIOC_QUERYCAP, &cap) == -1) {
      if (errno == EINVAL) {
        LOG4CXX_FATAL(logger_, "'" << dev_name_ << "' is no V4L2 device");
        exit(EXIT_FAILURE);
      } else {
        errno_throw_("VIDIOC_QUERYCAP");
      }
    }
    LOG4CXX_INFO(logger_, "Querying '" << dev_name_ << "' was successfully, capabilities=0x" << std::hex << cap.capabilities);

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
      LOG4CXX_FATAL(logger_, dev_name_ << " is no video capture device");
      exit(EXIT_FAILURE);
    }

    LOG4CXX_INFO(logger_, "Device capability:\n" << V4LHumanizer::humanize_capability(cap));

    switch (io_method_) {
    case IOMethod::READ:
      if (!(cap.capabilities & V4L2_CAP_READWRITE)) {
        LOG4CXX_FATAL(logger_, "Unsupported I/O method");
        exit(EXIT_FAILURE);
      }
      break;

    case IOMethod::MMAP:
    case IOMethod::USERPTR:
      if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        LOG4CXX_FATAL(logger_, "Unsupported I/O method");
        exit(EXIT_FAILURE);
      }
      break;
    }

    /* Select video input, video standard and tune here. */
    clear_(cropcap);

    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (xioctl_(fd_, VIDIOC_CROPCAP, &cropcap) == 0) {
      crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      crop.c    = cropcap.defrect; /* reset to default */

      if (xioctl_(fd_, VIDIOC_S_CROP, &crop) == -1) {
        switch (errno) {
        case EINVAL:
          /* Cropping not supported. */
          break;
        default:
          /* Errors ignored. */
          break;
        }
      }
    } else {
      /* Errors ignored. */
    }

    clear_(fmt);

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (force_format_) {
      LOG4CXX_INFO(logger_, "Set H264");
      fmt.fmt.pix.width       = 640; //replace
      fmt.fmt.pix.height      = 480; //replace
      fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_H264; //replace
      fmt.fmt.pix.field       = V4L2_FIELD_ANY;

      if (xioctl_(fd_, VIDIOC_S_FMT, &fmt) == -1)
        errno_throw_("VIDIOC_S_FMT");

      /* Note VIDIOC_S_FMT may change width and height. */
    } else {
      /* Preserve original settings as set by v4l2-ctl for example */
      if (xioctl_(fd_, VIDIOC_G_FMT, &fmt) == -1)
        errno_throw_("VIDIOC_G_FMT");
    }

    /* Buggy driver paranoia. */
    min = fmt.fmt.pix.width * 2;
    if (fmt.fmt.pix.bytesperline < min)
      fmt.fmt.pix.bytesperline = min;
    min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
    if (fmt.fmt.pix.sizeimage < min)
      fmt.fmt.pix.sizeimage = min;

    switch (io_method_) {
    case IOMethod::READ:
      init_read_(fmt.fmt.pix.sizeimage);
      break;

    case IOMethod::MMAP:
      init_mmap_();
      break;

    case IOMethod::USERPTR:
      init_userptr_(fmt.fmt.pix.sizeimage);
      break;
    }

    LOG4CXX_INFO(logger_, "Device '" << dev_name_ << "' was initialized successfully");
  }

  void uninit_device(void)
  {
    unsigned int i;

    switch (io_method_) {
    case IOMethod::READ:
      delete static_cast<char const *>(buffers_[0].start);
      break;

    case IOMethod::MMAP:
      for (i = 0; i < buffers_count_; ++i)
        if (-1 == munmap(buffers_[i].start, buffers_[i].length))
          errno_throw_("munmap");
      break;

    case IOMethod::USERPTR:
      for (i = 0; i < buffers_count_; ++i)
        delete static_cast<char const *>(buffers_[i].start);
      break;
    }

    buffers_.reset();

    LOG4CXX_INFO(logger_, "Device '" << dev_name_ << "' was uninitialized successfully");
  }

  void start_capturing() {
    unsigned int i;
    enum v4l2_buf_type type;

    switch (io_method_) {
    case IOMethod::READ:
      // Nothing to do.
      break;

    case IOMethod::MMAP:
      for (i = 0; i < buffers_count_; ++i) {
        struct v4l2_buffer buf;

        clear_(buf);
        buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index  = i;

        if (xioctl_(fd_, VIDIOC_QBUF, &buf) == -1)
          errno_throw_("VIDIOC_QBUF");
      }
      type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      if (xioctl_(fd_, VIDIOC_STREAMON, &type))
        errno_throw_("VIDIOC_STREAMON");
      break;

    case IOMethod::USERPTR:
      for (i = 0; i < buffers_count_; ++i) {
        struct v4l2_buffer buf;

        clear_(buf);
        buf.type      = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory    = V4L2_MEMORY_USERPTR;
        buf.index     = i;
        buf.m.userptr = (unsigned long)buffers_[i].start;
        buf.length    = buffers_[i].length;

        if (xioctl_(fd_, VIDIOC_QBUF, &buf) == -1)
          errno_throw_("VIDIOC_QBUF");
      }
      type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      if (xioctl_(fd_, VIDIOC_STREAMON, &type) == -1)
        errno_throw_("VIDIOC_STREAMON");
      break;
    }
  }

  void stop_capturing() {
    enum v4l2_buf_type type;

    switch (io_method_) {
    case IOMethod::READ:
      // Nothing to do.
      break;

    case IOMethod::MMAP:
    case IOMethod::USERPTR:
      type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      if (xioctl_(fd_, VIDIOC_STREAMOFF, &type) == -1)
        errno_throw_("VIDIOC_STREAMOFF");
      break;
    }
  }

  void operator () () {
    int count;

    count = frame_count_;

    while (count != 0) {
      for (;;) {
        fd_set fds;
        struct timeval tv;
        int r;

        FD_ZERO(&fds);
        FD_SET(fd_, &fds);

        tv.tv_sec  = 2;
        tv.tv_usec = 0;

        r = select(fd_ + 1, &fds, NULL, NULL, &tv);

        if (r == -1) {
          if (errno == EINTR)
            continue;
          errno_throw_("select");
        }

        if (r == 0) {
          LOG4CXX_FATAL(logger_, "select timeout");
          exit(EXIT_FAILURE);
        }

        if (read_frame_())
          break;
        /* EAGAIN - continue select loop. */
      }
      if (count > 0)
        --count;
    }
  }
private:
  void errno_throw_(char const *msg) {
    LOG4CXX_FATAL(logger_, msg << " error " << errno << ", " << strerror(errno));
    throw std::runtime_error(msg);
  }

  int xioctl_(int fh, int request, void *arg) {
    int r;

    do {
      r = ioctl(fh, request, arg);
    } while (r == -1 && errno == EINTR);

    return r;
  }

  template <class T>
  void clear_(T x) {
    bzero(&x, sizeof(x));
  } 

  template <class... Args>
  void wrong_(char const *str, Args... args) {
    fprintf(stderr, str, args...);
    exit(EXIT_FAILURE);
  }

  void init_read_(unsigned int buffer_size) {
    LOG4CXX_INFO(logger_, "Init " << dev_name_ << " with IOMethod::READ, buffer_size=" << buffer_size);

    buffers_.reset(new(std::nothrow) Buffer[1]);

    if (!buffers_) {
      LOG4CXX_FATAL(logger_, "Out of memory");
      exit(EXIT_FAILURE);
    }

    buffers_[0].length = buffer_size;
    buffers_[0].start  = new(std::nothrow) char[buffer_size];

    if (!buffers_[0].start) {
      LOG4CXX_FATAL(logger_, "Out of memory");
      exit(EXIT_FAILURE);
    }
  }

  void init_mmap_() {
    LOG4CXX_INFO(logger_, "Init " << dev_name_ << " with IOMethod::MMAP");
    struct v4l2_requestbuffers req;

    clear_(req);
    req.count  = 4;
    req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (xioctl_(fd_, VIDIOC_REQBUFS, &req) == -1) {
      if (errno == EINVAL) {
        LOG4CXX_FATAL(logger_, dev_name_ << " does not support memory mapping");
        exit(EXIT_FAILURE);
      } else {
        errno_throw_("VIDIOC_REQBUFS");
      }
    }

    if (req.count < 2) {
      LOG4CXX_FATAL(logger_, "Insufficient buffer memory on " << dev_name_);
      exit(EXIT_FAILURE);
    }

    buffers_.reset(new(std::nothrow) Buffer[req.count]);

    if (!buffers_) {
      LOG4CXX_FATAL(logger_, "Out of memory");
      exit(EXIT_FAILURE);
    }

    for (buffers_count_ = 0; buffers_count_ < req.count; ++buffers_count_) {
      struct v4l2_buffer buf;

      clear_(buf);

      buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      buf.memory      = V4L2_MEMORY_MMAP;
      buf.index       = buffers_count_;

      if (xioctl_(fd_, VIDIOC_QUERYBUF, &buf) == -1)
        errno_throw_("VIDIOC_QUERYBUF");

      buffers_[buffers_count_].length = buf.length;
      buffers_[buffers_count_].start =
            ::mmap(nullptr, // start anywhere
                   buf.length,
                   PROT_READ | PROT_WRITE, // required
                   MAP_SHARED, // recommended
                   fd_, buf.m.offset);

      if (buffers_[buffers_count_].start == MAP_FAILED)
        errno_throw_("mmap");
    }
  }

  void init_userptr_(unsigned int buffer_size) {
    LOG4CXX_INFO(logger_, "Init " << dev_name_ << " with IOMethod::USERPTR, buffer_size=" << buffer_size);
    
    struct v4l2_requestbuffers req;

    clear_(req);

    req.count  = 4;
    req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_USERPTR;

    if (xioctl_(fd_, VIDIOC_REQBUFS, &req) == -1) {
      if (errno == EINVAL) {
        LOG4CXX_FATAL(logger_, dev_name_ << " does not support user pointer i/o");
        exit(EXIT_FAILURE);
      } else {
        errno_throw_("VIDIOC_REQBUFS");
      }
    }

    buffers_.reset(new(std::nothrow) Buffer[4]);

    if (!buffers_) {
      LOG4CXX_FATAL(logger_, "Out of memory");
      exit(EXIT_FAILURE);
    }

    for (buffers_count_ = 0; buffers_count_ < 4; ++buffers_count_) {
      buffers_[buffers_count_].length = buffer_size;
      buffers_[buffers_count_].start = new(std::nothrow) char[buffer_size];

      if (!buffers_[buffers_count_].start) {
        LOG4CXX_FATAL(logger_, "Out of memory");
        exit(EXIT_FAILURE);
      }
    }
  }

  int read_frame_() {
    struct v4l2_buffer buf;
    long i;

    switch (io_method_) {
    case IOMethod::READ:
      if (read(fd_, buffers_[0].start, buffers_[0].length) == -1) {
        switch (errno) {
        case EAGAIN:
          return 0;

        case EIO:
        // Could ignore EIO, see spec.

        // fall through

        default:
          errno_throw_("read");
        }
      }

      process_image_(buffers_[0].start, buffers_[0].length);
      break;

    case IOMethod::MMAP:
      clear_(buf);

      buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      buf.memory = V4L2_MEMORY_MMAP;

      if (xioctl_(fd_, VIDIOC_DQBUF, &buf) == -1) {
        switch (errno) {
        case EAGAIN:
          return 0;

        case EIO:
        // Could ignore EIO, see spec.

        // fall through

        default:
          errno_throw_("VIDIOC_DQBUF");
        }
      }

      assert(buf.index < buffers_count_);

      process_image_(buffers_[buf.index].start, buf.bytesused);

      if (xioctl_(fd_, VIDIOC_QBUF, &buf) == -1)
        errno_throw_("VIDIOC_QBUF");
      break;

    case IOMethod::USERPTR:
      clear_(buf);

      buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      buf.memory = V4L2_MEMORY_USERPTR;

      if (xioctl_(fd_, VIDIOC_DQBUF, &buf) == -1) {
        switch (errno) {
        case EAGAIN:
          return 0;

        case EIO:
        // Could ignore EIO, see spec.

        // fall through

        default:
          errno_throw_("VIDIOC_DQBUF");
        }
      }

      for (i = 0; i < buffers_count_; ++i)
        if (buf.m.userptr == (unsigned long)buffers_[i].start
            && buf.length == buffers_[i].length)
          break;

      assert(i < buffers_count_);

      process_image_((void *)buf.m.userptr, buf.bytesused);

      if (xioctl_(fd_, VIDIOC_QBUF, &buf) == -1)
        errno_throw_("VIDIOC_QBUF");
      break;
    }

    return 1;
  }

  void process_image_(void const *p, int const size) {
    frame_number_++;

    // char filename[15];
    // sprintf(filename, "frame-%d.raw", frame_number_);
    // FILE *fp = fopen(filename, "wb");
    // fwrite(p, size, 1, fp);
    // fflush(fp);
    // fclose(fp);

    if (dump_input_) {
      char const filename[] = "video.raw";

      LOG4CXX_DEBUG(logger_, "Writting frame " << p << " size=" << size << " to " << filename);
      FILE *fp = fopen(filename, "ab");
      fwrite(p, size, 1, fp);
      fflush(fp);
      fclose(fp);
    }

    LOG4CXX_DEBUG(logger_, "Push frame " << p << " size=" << size << " to the image bus.");
    image_bus_.push_buffer(p, size);
  }

private:
  log4cxx::LoggerPtr logger_;

  std::string dev_name_;
  IOMethod    io_method_;
  ImageBus   &image_bus_;
  int         fd_;
  std::unique_ptr<Buffer[]> buffers_;
  unsigned                  buffers_count_ = 0;
  int         dump_input_   = false;
  int         frame_count_  = -1;
  int         frame_number_ = 0;
  bool        force_format_ = false;
};
