#pragma once

#include <iostream>

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <algorithm>

#include <log4cxx/logger.h>
#include <log4cxx/level.h>
#include <log4cxx/patternlayout.h>

#include "buffer.hxx"

struct Image {
  Image(char const *p, unsigned int const s)
  : size(s)
  {
    start.reset(new char[s]);
    ::memcpy(start.get(), p, size);
  }

  Image(Buffer const &buffer)
  : size(buffer.length)
  {
    start.reset(new char[buffer.length]);
    ::memcpy(start.get(), buffer.start, size);
  }

  Image(Image &&other)
  : start(std::move(other.start))
  , size(other.size)
  {
    other.size = 0;
  }
  
  std::unique_ptr<char []> start;
  unsigned int             size = 0;
};

struct Subscriber {
public:
  Subscriber(int const fd, std::string const &description)
  : fd_(fd)
  , description_(description)
  {}

  void write_image(Image const &image) {
    fd_set fds;
    struct timeval tv;
    int r;

    FD_ZERO(&fds);
    FD_SET(fd_, &fds);

    tv.tv_sec  = 5;
    tv.tv_usec = 0;

    r = select(fd_ + 1, NULL, &fds, NULL, &tv);

    if (r <= 0)
      return;

    ::send(fd_, image.start.get(), image.size, MSG_NOSIGNAL);
  }

  friend bool operator == (Subscriber const &left, Subscriber const &right) {
    return left.fd_ == right.fd_;
  }
  
  int fd() const { return fd_; }
  std::string const & description() const { return description_; }

private:
  int         fd_;
  std::string description_;
};

class ImageBus {
public:
  ImageBus()
  : logger_(log4cxx::Logger::getLogger("ImageBus"))
  {
    logger_->setLevel(log4cxx::Level::getInfo());
    logger_->addAppender(new log4cxx::ConsoleAppender(new log4cxx::PatternLayout("%d{%Y-%m-%d %H:%M:%S} %-5p %c %x - %m%n")));
  }

  void push_buffer(Buffer const &buffer) {
    push_buffer(buffer.start, buffer.length);
  }
  
  void push_buffer(void const *p, int const size) {
    std::unique_ptr<Image> image(new Image(reinterpret_cast<char const *>(p), size));
    {
      std::unique_lock<std::mutex> lock(queue_mutex_);
      queue_.emplace(std::move(image));
      while (queue_.size() > 1)
        queue_.pop();
    }
    
    queue_cond_.notify_all();
  }

  std::unique_ptr<Image> pop_image() {
    std::unique_lock<std::mutex> lock(queue_mutex_);
    if (queue_.empty())
      return nullptr;
    std::unique_ptr<Image> image(std::move(queue_.front()));
    queue_.pop();
    return std::move(image);
  }

  void add_subscriber(Subscriber const &subscriber) {
    std::unique_lock<std::mutex> lock(subscribers_mutex_);
    subscribers_.emplace_back(subscriber);
  }

  void remove_subscriber(int const fd) {
    std::unique_lock<std::mutex> lock(subscribers_mutex_);
    auto iter = std::remove_if(subscribers_.begin(), subscribers_.end(), [fd](Subscriber const &subscriber) {
      return subscriber.fd() == fd;
    });
    if (iter != subscribers_.end())
      subscribers_.erase(iter);
  }
  
  void operator () () {
    for (;;) {
      {
        std::unique_lock<std::mutex> lock(queue_mutex_);
        queue_cond_.wait(lock);
      }
      
      std::unique_ptr<Image> image;
      while (image = std::move(pop_image())) {
        std::unique_lock<std::mutex> lock(subscribers_mutex_);
        for (auto &subscriber : subscribers_) {
          LOG4CXX_DEBUG(logger_, "Write image to subscriber " << subscriber.description() << " size=" << image->size);
          subscriber.write_image(*image.get());
        }
      }
    }
  }

private:
  log4cxx::LoggerPtr logger_;

  std::mutex                         queue_mutex_;
  std::condition_variable            queue_cond_;
  std::queue<std::unique_ptr<Image>> queue_;
  
  std::mutex              subscribers_mutex_;
  std::vector<Subscriber> subscribers_;
};
