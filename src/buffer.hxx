#pragma once

struct Buffer {
  void        *start;
  std::size_t  length;
};
