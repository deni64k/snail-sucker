// #include <iostream>
// #include <cstdlib>
// #include <cctype>
// #include <exception>
// #include <stdexcept>
// #include <memory>
#include <string>
#include <thread>

#include <getopt.h>
#include <signal.h>

#include <log4cxx/logger.h>
#include <log4cxx/level.h>
#include <log4cxx/consoleappender.h>
#include <log4cxx/patternlayout.h>

#include "server_tcp.hxx"
#include "sucker.hxx"

namespace {
  char const short_options[] = "d:hmruofcp:";

  struct option const long_options[] = {
    {"device", required_argument, nullptr, 'd'},
    {"help",   no_argument,       nullptr, 'h'},
    {"mmap",   no_argument,       nullptr, 'm'},
    {"read",   no_argument,       nullptr, 'r'},
    {"userp",  no_argument,       nullptr, 'u'},
    {"output", no_argument,       nullptr, 'o'},
    {"format", no_argument,       nullptr, 'f'},
    {"count",  required_argument, nullptr, 'c'},
    {"port",   required_argument, nullptr, 'p'},
    {0, 0, 0, 0}
  };

  void usage(FILE *fp, int argc, char **argv) {
    fprintf(fp,
            "Usage: %s [options]\n\n"
            "Version 1.3\n"
            "Options:\n"
            "-d | --device name   Video device name [/dev/video0]\n"
            "-h | --help          Print this message\n"
            "-m | --mmap          Use memory mapped buffers [default]\n"
            "-r | --read          Use read() calls\n"
            "-u | --userp         Use application allocated buffers\n"
            "-o | --output        Outputs stream to stdout\n"
            "-f | --format        Force format to 640x480 H264\n"
            "-c | --count         Number of frames to grab [unlimited]\n"
            "-p | --port          Port to listen [8000]\n"
            "",
            argv[0]);
  }

  void errno_exit(const char *s) {
    fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
    exit(EXIT_FAILURE);
  }
}

int main(int argc, char **argv)
{
  // ::signal(SIGPIPE, SIG_IGN);

  log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger("sucker");
  logger->setLevel(log4cxx::Level::getAll());
  logger->addAppender(new log4cxx::ConsoleAppender(new log4cxx::PatternLayout("%d{%Y-%m-%d %H:%M:%S} %-5p %c %x - %m%n")));
  LOG4CXX_INFO(logger, "Some info for you.");

  std::string dev_name    = "/dev/video0";
  IOMethod    io_method   = IOMethod::MMAP;
  long        frame_count = -1;
  int         port        = 8000;

  for (;;) {
    int idx;
    int c;

    c = getopt_long(argc, argv, short_options, long_options, &idx);

    if (c == -1)
      break;

    switch (c) {
    case 0: /* getopt_long() flag */
      break;

    case 'd':
      dev_name = optarg;
      break;

    case 'h':
      usage(stdout, argc, argv);
      exit(EXIT_SUCCESS);

    case 'm':
      io_method = IOMethod::MMAP;
      break;

    case 'r':
      io_method = IOMethod::READ;
      break;

    case 'u':
      io_method = IOMethod::USERPTR;
      break;

    // case 'o':
    //   out_buf++;
    //   break;

    // case 'f':
    //   force_format++;
    //   break;

    case 'c':
      frame_count = std::strtol(optarg, nullptr, 0);
      if (errno)
        errno_exit(optarg);
      break;

    case 'p':
      port = std::strtol(optarg, nullptr, 0);
      if (errno)
        errno_exit(optarg);
      break;

    default:
      usage(stderr, argc, argv);
      exit(EXIT_FAILURE);
    }
  }

  ImageBus image_bus;

  std::thread thread_server_tcp([=, &image_bus]() {
    ServerTCP server_tcp(image_bus, port);
    server_tcp();
  });

  std::thread thread_sucker([=, &image_bus]() {
    Sucker sucker(dev_name, io_method, image_bus);
    sucker.open_device();
    sucker.init_device();
    sucker.start_capturing();
    sucker();
    sucker.stop_capturing();
    sucker.uninit_device();
    sucker.close_device();
  });

  image_bus();

  thread_server_tcp.join();
  thread_sucker.join();

  return 0;
}
